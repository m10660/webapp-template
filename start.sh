#!/bin/bash
# set -x

echo "Source settings"
. config.ini
export DATABASE_URL
export NODE_OPTIONS=--max-http-header-size=16384

PWD=`pwd`
POSTGREST=${PWD}/postgrest
QUASAR=${PWD}/quasar
MIGRATE=${PWD}/migrate
WORKER=${PWD}/worker

echo "Stopping servers"
./stop.sh

echo "Migrating database"
(cd ${MIGRATE} && yarn install && npm run migrate up)

echo "Starting Postgrest API Server"
${POSTGREST}/postgrest ${POSTGREST}/api.conf &

echo "Starting Graphile worker"
(cd ${WORKER} && yarn install && yarn start) &

echo "Installing Quasar"
yarn global add @quasar/cli@1.0.2

echo "Starting Quasar development server"

while getopts ":f" opt; do
  case ${opt} in
    f ) (cd ${QUASAR} && rm -Rf node_modules && rm -f yarn.lock && yarn cache clean)
      ;;
    \? ) echo "Usage: ./start.sh [-f]"
      ;;
  esac
done
(cd ${QUASAR} && yarn install && quasar dev) &
