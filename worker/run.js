const yargs = require("yargs");
const argv = yargs
  // .command('Run ETL job')
  .usage("Usage: $0 <job> [options]")
  .command("Run ETL job with optional option")
  .demandCommand(1)
  .help()
  .alias("help", "h").argv;
console.log(`Run ${argv._} ...`);
const job = require(`./jobs/${argv._[0]}`);
const args = argv._.slice(1).length > 0 ? argv._.slice(1) : null;
job.run(args).then(() => {
  setImmediate(() => process.exit);
});
