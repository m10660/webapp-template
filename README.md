The web app template project is a ready-to-use project to fork to create a new web application. The project uses the standard structure and includes all the necessary elements so that the developer can start its implementation without having to recreate the entire application. 

Good development :sunglasses:

[[_TOC_]]

# Introduction

`To complete`

## Components

### Production

```plantuml
@startuml
cloud browser [
<&browser> Browser
]
cloud ad[
<&browser> Active Directory
]

folder "docker_devops" as portainer_devops {
frame "keycloak" {
node keycloak[
<&key> Keycloak
]
}
frame "rundeck" as rundeck_stack {
node rundeck_svc[
<&key> rundeck
]
}
}
folder "portainer.docker.corp.org.ebrc.local" as portainer {
frame "traefik" {
node traefik[
<&map> Traefik
]
}
frame "prd_<app>" {

  node "nginx" as nginx_svc{  
    collections quasar_dist[
  /quasar/dist/spa
  ]
rectangle "<&cog> nginx" as nginx
 }
  node "builder" as builder_svc{
rectangle "<&cog> node-pg-migrate" as migrate
 rectangle quasar_build[
  <&cog> quasar build
  ]
 collections migrations[
  /migrate/migrations
  ]
  collections quasar[
  /quasar/src
  ]
}
  node "db"  as dbsvc{
  database "postgres" as db
  }
  node postgrest_svc[  
  <&globe> postgrest
  ] 
  node "worker" as worker_svc{
 rectangle graphile[
  <&cog> graphile-worker
  ]
 rectangle runner[
  <&cog> /worker/run.js
  ]
 collections jobs[
  /worker/jobs
  ]
 collections tasks[
  /worker/tasks
  ]

}


  node gatekeeper[  
  <&ban> Gatekeeper
  ]



}

}
browser -l-> traefik : https://<app>.docker.corp.org.ebrc.local

gatekeeper -d-> keycloak : https:443
traefik --> nginx : http://prd_<app>_nginx
postgrest_svc -l-> db: sql:5432
graphile -d-> db: sql:5432
migrate -d-> db: sql:5432
runner -r-> db : sql:5432
jobs .. runner
tasks .. graphile
quasar .. quasar_build
quasar_build -d-> quasar_dist:build
nginx .r. quasar_dist :/
migrations .. migrate
nginx -d-> gatekeeper

nginx -d-> postgrest_svc : /api -> http://postgrest:3000
gatekeeper -u-> browser : redirect
keycloak --> ad : ldap
rundeck_svc -u-> runner : ssh

@enduml
```

```plantuml
@startuml
rectangle "Legend" as legend{
folder "Docker host"
frame "Swarm stack"
node "Swarm service"
collections "files"
rectangle "executable"
database "Database"
}
@enduml
```

### Development

```plantuml
@startuml
cloud browser [
<&browser> Browser
]
cloud ad[
<&browser> Active Directory
]

folder "portainer.docker-dev.corp.org.ebrc.local" as portainer {
frame "traefik" {
node traefik[
<&map> Traefik
]
}
frame "dev_keycloak" {
node keycloak[
<&key> Keycloak
]
}




frame "dev_xxx" {

  node nginx[  
  <&globe> nginx
  ] 
  node "dev" {
 collections jobs[
  /worker/jobs
  ]
  collections quasar[
  /quasar/src
  ]
 collections tasks[
  /worker/tasks
  ]
 collections migrations[
  /migrate/migrations
  ]

rectangle "<&cog> /migrate/migrate" as migrate
rectangle "<&cog> /start.sh" {

 rectangle postgrest[
  <&cog> /postgrest/postgrest
  ]
 rectangle quasar_dev[
  <&cog> quasar dev
  ]
 rectangle graphile[
  <&cog> /worker/graphile-worker
  ]
}
rectangle "<&cog> /worker/daily.sh" {
 rectangle runner[
  <&cog> /worker/run.js
  ]
}
}



  node gatekeeper[  
  <&ban> Gatekeeper
  ]



}
frame "dev_databases" { 
node "dev_databases_<version>" as db_svc {
database db [
<&database> <app>_xxx
]
}
} 
}
browser --> traefik : https://dev_xxx.docker-dev.corp.org.ebrc.local

gatekeeper --> keycloak : https:443
traefik --> nginx : http://dev_xxx_nginx
postgrest --> db: sql:5432
graphile -r-> db: sql:5432
migrate -r-> db: sql:5432
runner --> db
jobs .. runner
tasks .. graphile
quasar .. quasar_dev
migrations .. migrate
nginx -l-> gatekeeper
nginx --> quasar_dev : / -> http://dev:8080
nginx --> postgrest : /api -> http://dev:3000
gatekeeper --> browser : redirect
keycloak --> ad : ldap
@enduml
```


# Get started

To use this template project, the main steps are:
- copy the template project by forking it into a new project,
- clone the new project,
- create a database in development
- and configure the application to start it.

## Fork the project

Click on the `Fork` button at the top right of the page. The `Fork project` page appears.
Complete the `Project Name` field with the name of your project.
Select `webapps` namespace. Then modify the `Description` field.
Click on the `Fork project` button.

The new project is now available in the group https://gitlab.ebrc.local/webapps

## Clone the new project
On the new project page, copy the `Clone with SSH` link by clicking the `Clone` button.
Open a terminal in `VSCode` and go to the `/root/webapps` directory. Create this directory if it does not exist.
Launch the `git clone` command with the link copied previously.
Example:
```
[root@dev_fpe webapps]# git clone ssh://git@gitlab.ebrc.local:2222/webapps/my-web-app.git
```
## Create the development database
The application cannot work without a database.
Go into DBeaver to create it in development server. The naming standard for databases in development is `<application name>_<trigram>`. Example: `dwh_fpe`.

## Configure application
In the development environment, the web application uses a configuration file instead of environment variables.
This file is called by the service startup scripts in order to declare the environment variables necessary for the proper functioning of the application.
The essential environment variable is the connection to the database.
So, create the `config.ini` file under the root of the project with at least the line of information for connecting to the database that you created previously.
example:
```
# config.ini

export DATABASE_URL=postgres://postgres:postgres@postgres14_tds_mysql_fdw:5432/template_fpe
```
Then, you can add all the environment variables necessary for the operation of the application to be developed.


## Start/stop the application
When the config.ini file is completed, to start the application, run the `start.sh` bash script.
```
[root@dev_fpe template]# ./start.sh
```
The start command displays this message. 

 ```
 DONE  Compiled successfully in 14390ms                                                                                                                                                                       8:55:43 AM


 N  App dir........... /root/template/quasar
    App URL........... http://localhost:8080/
    Dev mode.......... spa
    Pkg quasar........ v1.18.9
    Pkg @quasar/app... v2.2.12
    Transpiled JS..... yes (Babel) - includes IE11 support
  
ℹ ｢wds｣: Project is running at http://0.0.0.0:8080/
ℹ ｢wds｣: webpack output is served from /
ℹ ｢wds｣: 404s will fallback to /index.html
```
Check if the application is available on the development container at the following url with your `trigram` for XXX: https://dev_XXX.docker-dev.corp.org.ebrc.local

Example : https://dev_fpe.docker-dev.corp.org.ebrc.local

And to stop the application, run the `stop.sh` bash script.
```
[root@dev_fpe template]# ./stop.sh
```

# Structure files

`To complete`

## Migrate

## Worker

## Quasar

## Others

# How to

## Lint

`Linting` is a practice that aims to improve the quality of the code and therefore its recovery and maintainability.

The `lint.sh` script is available to linter all project code.

```
[root@dev_fpe my-web-app]# ./lint.sh 
yarn run v1.22.5
$ eslint --ext .js,.vue ./ --fix
Done in 2.01s.
yarn run v1.22.5
$ eslint --ext .js . --fix
Done in 0.59s.
yarn run v1.22.5
$ eslint --ext .js . --fix
```

**Don't forget to linter before committing your code.**

## Remove all local branches not on remote
Chances are you've had a situation where you wanted to delete all non-remote local branches. This often happens to developers, especially in large projects.
The `clean_git_branches.sh` script is available to remove all Local branches not on remote.

```
[root@dev_fpe my-web-app]# ./clean_git_branches.sh 
```

## Migrate database
`To complete`

# Links
## Web user interface
|||
|---|---|
|[VueJS 2](https://v2.vuejs.org/)|The Progressive JavaScript Framework. VueJS is a library that allows you to create interactive web interfaces based on the principle of web components.|
| [Quasar v1](https://v1.quasar.dev/)| Effortlessly build high-performance & high-quality Vue.js 2 user interfaces in record time |
|[Highcharts](https://www.highcharts.com/)| Rock-solid and incredibly flexible charting library made for developers.|
|[Excel Editor](https://github.com/cscan/vue-excel-editor)| Vue2 plugin for displaying and editing the array-of-object in Excel style.|
|[Material Icons](https://fonts.google.com/icons)|Material design system icons are simple, modern, friendly, and sometimes quirky.|
## Database
|||
|---|---|
|[PostgreSQL](https://www.postgresql.org/docs/current/index.html)|Powerful, open source object-relational database system.|
|[node-pg-migrate](https://salsita.github.io/node-pg-migrate/#/)|Node.js database migration management built exclusively for postgres.|
|[PostgREST](https://postgrest.org/en/stable/)|Standalone web server that turns your PostgreSQL database directly into a RESTful API. The structural constraints and permissions in the database determine the API endpoints and operations.|
|[MassiveJS](https://massivejs.org/)|Data mapper for Node.js that goes all in on PostgreSQL and embraces the power and flexibility of SQL and the relational model.|
## Worker
|||
|---|---|
|[Graphile Worker](https://github.com/graphile/worker)|Job queue for PostgreSQL running on Node.js - allows you to run jobs (e.g. sending emails, performing calculations, generating PDFs, etc) in the background so that your HTTP response/application code is not held up.|
## Other NodeJS and javascript libraries
|||
|---|---|
| [Moment.js](https://momentjs.com/) | Parse, validate, manipulate,and display dates and times in JavaScript.|
| [Lodash](https://lodash.com/)| A modern JavaScript utility library delivering modularity, performance & extras.|
|[Axios](https://axios-http.com/)| Axios is a simple promise based HTTP client for the browser and node.js. Axios provides a simple to use library in a small package with a very extensible interface.|

# Tutorials and videos
## VueJS 2
|||
|---|---|
|[The Net Ninja](https://www.youtube.com/watch?v=5LYrN_cAJoA&list=PL4cUxeGkcC9gQcYgjhBoeQH7wiAyZNrYa)| Best VueJS tutorial for beginners I can say. Explanation of the concepts is simple with good examples. Most of the important topics are covered in the series.|
|[Grafikart](https://grafikart.fr/formations/vuejs)| A good VueJS tutorial in french.|
## Quasar v1
|||
|---|---|
|[Program With Erik - Getting Started](https://www.youtube.com/watch?v=5LYrN_cAJoA&list=PL4cUxeGkcC9gQcYgjhBoeQH7wiAyZNrYa)||
|[Program With Erik - Let's Build An App!](https://grafikart.fr/formations/vuejs)| In this video I create an app from start to finish using Quasar and many of it's components. Including loading spinners, cards, sections and more!|
## PostgREST
|||
|---|---|
|[Building a Contacts List with PostgREST and Vue.js: The Database & API](https://www.youtube.com/watch?v=iHtsALtD5-U)|






