const fs = require("fs");
const path = require("path");

const func = key =>
  fs.readFileSync(path.resolve(__dirname, `${key.toString()}.sql`), "utf8");

module.exports = func;
