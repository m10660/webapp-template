[see node-pg-migrate documentation](https://github.com/salsita/node-pg-migrate#docs)

1. Specify your database connection information in  configuration file  : ../source.ini
2. Then run `npm run migrate create my first migration` to create a new migration file . It will create file `xxx_my-first-migration.js` in migrations folder. Open it and change contents. 
see [example](https://github.com/salsita/node-pg-migrate#quick-example)
3. And finally run `npm run migrate up` to migrate database

**The following are the available commands:**

- `npm run migrate create {migration-name}` - creates a new migration file with the name you give it. Spaces and underscores will be replaced by dashes and a timestamp is prepended to your file name.
- `npm run migrate up` - runs all up migrations from the current state.
- `npm run migrate up {N}` - runs N up migrations from the current state.
- `npm run migrate down` - runs a single down migration.
- `npm run migrate down {N}` - runs N down migrations from the current state.
- `npm run migrate redo` - redoes last migration (runs a single down migration, then single up migration).
- `npm run migrate redo {N}` - redoes N last migrations (runs N down migrations, then N up migrations).

[CLI Usage](https://github.com/salsita/node-pg-migrate/blob/master/docs/cli.md)

[Defining migrations](https://github.com/salsita/node-pg-migrate/blob/master/docs/migrations.md)
