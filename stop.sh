#!/bin/bash
# set -x

echo "Stopping Postgrest API Server"
# kill postgrest if running
pgrep -f 'postgrest' | while read -r line ; do
    kill -9 $line
done

echo "Stopping Quasar development server"

# kill quasar if running
pgrep -f 'quasar' | while read -r line ; do
    kill -9 $line
done

echo "Stopping Graphile worker"

# kill quasar if running
pgrep -f 'graphile-worker' | while read -r line ; do
    kill -9 $line
done